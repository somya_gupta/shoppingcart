var fruits=[{
	name:"apple",
	category:"fruits",
    price:50,
    quantity:0	
},
{
	name:"orange",
	category:"fruits",
    price:20,
    quantity:0	
},
{
	name:"pear",
	category:"fruits",
	price:20,
    quantity:0	
},
{
	name:"watermelon",
	category:"fruits",
	price:30,
    quantity:0	
},
{
	name:"banana",
	category:"fruits",
	price:20,
    quantity:0
},
{
	name:"pomegraname",
	category:"fruits",
	price:50,
    quantity:0	
},
{
	name:"papaya",
	category:"fruits",
	price:36,
    quantity:0	
},
{
	name:"kiwi",
	category:"fruits",
	price:70,
    quantity:0	
}];


var vegetables=[
{
	name:"spinach",
	category:"vegetables",
	price:19,
    quantity:0	
},
{
	name:"onion",
	category:"vegetables",
	price:24,
    quantity:0	
},
{
	name:"potato",
	category:"vegetables",
	price:28,
    quantity:0	
},
{
	name:"tomato",
	category:"vegetables",
	price:10,
    quantity:0	
},
{
	name:"cauliflower",
	category:"vegetables",
	price:19,
    quantity:0	
},
{
	name:"capsicum",
	category:"vegetables",
	price:100,
    quantity:0	
},


{
	name:"cucumber",
	category:"vegetables",
	price:24,
    quantity:0	
}];

var grocery=[
{
	name:"rice",
	category:"grocery",
	price:44,
    quantity:0	
},
{
	name:"salt",
	category:"grocery",
	price:18,
    quantity:0	
},
{
	name:"turmeric",
	category:"grocery",
	price:38,
    quantity:0	
},

{
	name:"wheat",
	category:"grocery",
	price:256,
    quantity:0	
},
{
	name:"gramflour",
	category:"grocery",
	price:256,
    quantity:0	
},
{
	name:"pulses",
	category:"grocery",
	price:256,
    quantity:0	
}
,
{
	name:"red chilli powder",
	category:"grocery",
	price:22,
    quantity:0	
},

{
	name:"vermicelli",
	category:"grocery",
	price:49,
    quantity:0
}];


var biscuits=[{
	name:"oreo",
	category:"biscuits",
	price:35,
    quantity:0
},

{
	name:"parleG",
	category:"biscuits",
	price:5,
    quantity:0
},
{
	name:"marie gold",
	category:"biscuits",
	price:10,
    quantity:0
},
{
	name:"good day",
	category:"biscuits",
	price:34,
    quantity:0
},
{
	name:"dark fantasy",
	category:"biscuits",
	price:56,
    quantity:0
},

{
	name:" cream biscuit",
	category:"biscuits",
	price:38,
    quantity:0
},
{
	name:"crunchy",
	category:"biscuits",
	price:71,
    quantity:0
},
{
	name:"monaco",
	category:"biscuits",
	price:47,
    quantity:0
}];


myvalue ="";
row=1;
function search()
{   
	myvalue="";
  
	document.getElementById("tableindex").style.visibility="visible";
    myvalue=myvalue+"<tr><th>Name</th><th>Category</th><th>price</th><th>Quantity</th></tr>";
    var search=document.getElementById("inputField").value;
    var reg=new RegExp(search,"g");
    fruits.forEach((fruit)=>{
        if(fruit.name.toLowerCase().match(reg))
            printRows(fruit);
    });
    vegetables.forEach((vegitable)=>{
        if(vegitable.name.toLowerCase().match(reg))
            printRows(vegitable);
    });
    grocery.forEach((groc)=>{
        if(groc.name.toLowerCase().match(reg))
            printRows(groc);
    });
    biscuits.forEach((biscuit)=>{
        if(biscuit.name.toLowerCase().match(reg))
            printRows(biscuit);
    });
    document.getElementById("tableindex").innerHTML=myvalue;

}
function printRows(object)
{
    myvalue = myvalue + "<tr><td id=name"+row+">"+object.name + "</td><td>" +object.category + "</td><td>" +
                     object.price + "</td><td><input type='number' id=row"+row+" name='quantity' value=0 min='0' max='5'></td></tr>";
    row++;
}
cartRow="<tr><th>Name</th><th>Quantity</th></tr>";
i=1;
function addToCart()
{   
    for(;i<row;){
        var qnt=document.getElementById("row"+i).value;
        if(qnt>0){
		   var itemName= document.getElementById("name"+i).innerHTML;
		   localStorage.setItem("name"+i,itemName);
		   localStorage.setItem("quantity"+i,qnt);	
           cartRow=cartRow+"<tr><td id='nameInCart"+i+"'>"+itemName+"</td><td id='quantityInCart"+i+"'>"+qnt+"</td></tr>";
	       i++;	
		}
	}   
    document.getElementById("cart").innerHTML=cartRow;
}

function clearElement(id){
    var Ref = document.getElementById(id);
	 if(Ref) Ref.parentNode.removeChild(Ref);
}

function check(){
	clearElement('tableindex');
	clearElement("addToCart");
	clearElement("reset");

	document.getElementById("checkouttable").style.visibility="visible";
	var itemNo=1;
    var tableLength = document.getElementById('cart').rows.length;
      while ( itemNo < tableLength )
      {
		var name=localStorage.getItem("name"+itemNo);
		var quantity=localStorage.getItem("quantity"+itemNo);	 
		searchIn(name,quantity);
        itemNo++;
	 }
	 lastRow="<tr><th>Grand Total</th><td colspan='3'></td><td style='border-top:solid 1px'>"+total+"</td></tr>";   
	 billRow=billRow+lastRow;
	 document.getElementById("checkouttable").innerHTML=billRow;
	
}

function searchIn(myItemName,myItemQnt){

    var reg=new RegExp(myItemName,"i");
    fruits.forEach((fruit)=>{
        if(fruit.name.match(reg)){
			fruit.quantity=myItemQnt;
			printbill(fruit);
            
        }
    });
    vegetables.forEach((vegitable)=>{
        if(vegitable.name.toLowerCase().match(reg)){
			vegitable.quantity=myItemQnt;
			printbill(vegitable);
            
        }
    });
    grocery.forEach((groc)=>{
        if(groc.name.toLowerCase().match(reg)){
			groc.quantity=myItemQnt;
			printbill(groc);
            
        }
    });
    biscuits.forEach((biscuit)=>{
        if(biscuit.name.toLowerCase().match(reg)){
			biscuit.quantity=myItemQnt;
			printbill(biscuit);
            
        }
	});
   
	
   

}
total=0;
billRow="<tr><th>Name</th><th>cetegory</th><th>MRP</th><th>Quantity</th><th>Price</th></tr>";
function printbill(myObject){
	var unitTotal=myObject.price*myObject.quantity;
	total=total+unitTotal;
	billRow=billRow+"<tr><td>"+myObject.name+"</td><td>"+myObject.category+"</td><td>"+
    myObject.price+"</td><td>"+myObject.quantity+"<td>"+unitTotal+"</td><tr/>";
}